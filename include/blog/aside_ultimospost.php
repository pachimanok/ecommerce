<aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title">Ultimos Posteos</h3>
                            <div class="media post_item">
                                <img src="img/post/post_1.png" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>La vida en armonia</h3>
                                    </a>
                                    <p>10 de Octubre, 2020</p>
                                </div>
                            </div>
                            <div class="media post_item">
                                <img src="img/post/post_2.png" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Comprar muebles de calidad.</h3>
                                    </a>
                                    <p>7 de Septiembre, 2020</p>
                                </div>
                            </div>
                            <div class="media post_item">
                                <img src="img/post/post_3.png" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Como cuidar los muebles en Cuarentena</h3>
                                    </a>
                                    <p>5 de Junio, 2020</p>
                                </div>
                            </div>
                            <div class="media post_item">
                                <img src="img/post/post_4.png" alt="post">
                                <div class="media-body">
                                    <a href="single-blog.html">
                                        <h3>Tendencias para el 2020</h3>
                                    </a>
                                    <p>23 de Marzo, 2020</p>
                                </div>
                            </div>
                        </aside>
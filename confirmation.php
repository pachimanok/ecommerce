<?php include('include/header.php');?>

  <!--================Home Banner Area =================-->
  <!-- breadcrumb start-->
  <section class="breadcrumb breadcrumb_bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="breadcrumb_iner">
            <div class="breadcrumb_iner_item">
              <h2>Confirmación de Pedido</h2>
              <p>Aveline Atelier</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- breadcrumb start-->

  <!--================ confirmation part start =================-->
  <section class="confirmation_part padding_top">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="confirmation_tittle">
            <span>Gracias. Hemos recibido su orden en instantes nos pondremos en contacto.</span>
          </div>
        </div>
        <div class="col-lg-6 col-lx-4">
          <div class="single_confirmation_details">
            <h4>Detalle de la Orden.</h4>
            <ul>
              <li>
                <p>Order id</p><span>: 602</span>
              </li>
              <li>
                <p>Fecha:</p><span>: Oct 03, 2017</span>
              </li>
              <li>
                <p>Totall</p><span>: $ 43100</span>
              </li>
              <li>
                <p>Metodo de Pago</p><span>: Mercado Pago</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-6 col-lx-4">
          <div class="single_confirmation_details">
            <h4>Datos de Envio:</h4>
            <ul>
              <li>
                <p>Destinatario:</p><span>: Juan Perez</span>
              </li>
              <li>
                <p>Consignatario:</p><span>: Alba Rivas</span>
              </li>
              <li>
                <p>Celular:</p><span>: 26127680076</span>
              </li>
              <li>
                <p>Email</p><span>: jperez@gmail.com</span>
              </li>
              <!--li>
                <p>Observaciones</p><span>: Subir hasta el 5to piso.</span>
              </li-->
              <!--li>
                <p>postcode</p><span>: 36952</span>
              </li-->
            </ul>
          </div>
        </div>
        <div class="col-lg-6 col-lx-4">
          <div class="single_confirmation_details">
            <h4>Direccion de Entrega: </h4>
            <ul>
              <li>
                <p>Direccion:</p><span>: Av. San Matin 1600.</span>
              </li>
              <li>
                <p>Localidad:</p><span>: Godoy Cruz</span>
              </li>
              <li>
                <p>Observaciones</p><span>: Subir al 5to Piso.</span>
              </li>
              <!--li>
                <p>postcode</p><span>: 36952</span>
              </li-->
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="order_details_iner">
            <h3>Detalle del Pedido</h3>
            <table class="table table-borderless">
              <thead>
                <tr>
                  <th scope="col" colspan="2">Producto</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                  <th>x02</th>
                  <th> <span>$720.00</span></th>
                </tr>
                <tr>
                  <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                  <th>x02</th>
                  <th> <span>$720.00</span></th>
                </tr>
                <tr>
                  <th colspan="2"><span>Pixelstore fresh Blackberry</span></th>
                  <th>x02</th>
                  <th> <span>$720.00</span></th>
                </tr>
                <tr>
                  <th colspan="3">Subtotal</th>
                  <th> <span>$2160.00</span></th>
                </tr>
                <tr>
                  <th colspan="3">shipping</th>
                  <th><span>flat rate: $50.00</span></th>
                </tr>
              </tbody>
              <!--tfoot>
                <tr>
                  <th scope="col" colspan="3">Quantity</th>
                  <th scope="col">Total</th>
                </tr>
              </tfoot-->
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ confirmation part end =================-->
<?php include('include/footer.php');?>